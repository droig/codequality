'use strict';

// This class is used for logins
class Login {
    constructor(hash) {
        this.sessions = [];
        this.users = [];
        this.passwords = [];
        Object.keys(hash).map(k => ({k, v: hash[k]})).map(e => {
                this.users = this.users.concat([e.k]);
            this.passwords = this.passwords.concat([e.v]);
        });
    }

    logout(user) {
        this.sessions.forEach((session, i) => {
                if (session === user) {
                this.sessions[i] = null;
            }
        });
        this.sessions = this.sessions.filter(session => session !== null);
    }

    // Checks if user exists
    userExists(user) {
        return this.users.indexOf(user) !== -1;
    }

    // Register user
    registerUser(user, password) {
        let lastIndex = this.users.length;
        this.users[lastIndex] = user;
        this.passwords[lastIndex] = password;
    }

    removeUser(user) {
        let index = this.users.indexOf(user);
        delete this.users[index];
        delete this.passwords[index];
    }

    checkPassword(user, password) {
        let index = this.users.indexOf(user);
        let passwordCorrect = this.passwords[index] === password;
        return passwordCorrect;
    }

    updatePassword(user, oldPassword, newPassword) {
        // First we check if the user exists
        if ( this.userExists(user) ) {
            let index = this.users.indexOf(user);
            if (this.passwords[index] === oldPassword) {
                this.passwords[index] = newPassword;
                return true;
            }
        }
        return false;
    }

    login(user, password) {
        if (this.checkPassword(user, password)) {
            this.sessions.push(user);
        }
    }
}

let registeredUsers = {
    user1: 'pass1',
    user2: 'pass2',
    user3: 'pass3'
};

let login = new Login(registeredUsers);

login.registerUser('user4', 'pass4');
login.login('user4', 'pass4');
login.updatePassword('user3', 'pass3', 'pass5');
login.login('user3', 'pass5');
login.logout('user4');
login.logout('user3');

console.log(login.sessions);